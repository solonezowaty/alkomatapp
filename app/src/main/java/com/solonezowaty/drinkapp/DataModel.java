package com.solonezowaty.drinkapp;

import android.provider.ContactsContract;

/**
 * Created by Jakub Solecki on 2018-12-18.
 */
public class DataModel {

    private int weightValue;
    private int numberOfBeers;
    private int sexVariable;

    public DataModel() {
    }

    public int getWeightValue() {
        return weightValue;
    }

    public void setWeightValue(int weightValue) {
        this.weightValue = weightValue;
    }

    public int getNumberOfBeers() {
        return numberOfBeers;
    }

    public void setNumberOfBeers(int numberOfBeers) {
        this.numberOfBeers = numberOfBeers;
    }

    public int getSexVariable() {
        return sexVariable;
    }

    public void setSexVariable(int sexVariable) {
        this.sexVariable = sexVariable;
    }

    public double calculatePromils(DataModel dataModel){
        double promils = 0;
        final int oneBeerInMililiters = 500;
        final double percentageOfAlcoholInOneBeer = 0.06;
        final double densityOfAlcohol = 0.79;

        if (dataModel.getSexVariable() == 1){

            double amountOfAlcohol = (((dataModel.getNumberOfBeers() * oneBeerInMililiters) * percentageOfAlcoholInOneBeer) * densityOfAlcohol);
            double bodilyFluids = 0.6 * dataModel.getWeightValue();
            promils = amountOfAlcohol/bodilyFluids;

        } else if (dataModel.getSexVariable() == 0){

            double amountOfAlcohol = (((dataModel.getNumberOfBeers() * oneBeerInMililiters) * percentageOfAlcoholInOneBeer) * densityOfAlcohol);
            double bodilyFluids = 0.7 * dataModel.getWeightValue();
            promils = amountOfAlcohol/bodilyFluids;
        }
        return promils;
    }
}
