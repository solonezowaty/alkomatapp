package com.solonezowaty.drinkapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText weightValueEditText;
    private EditText numberOfBeersEditText;
    private RadioGroup sexRadioGroup;
    private View calculatePromils;
    private DataModel dataModel = new DataModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getLayoutComponents();
        setOnClickListeners();
    }

    private void getLayoutComponents(){
        weightValueEditText = findViewById(R.id.weight_value);
        numberOfBeersEditText = findViewById(R.id.beers_value);
        sexRadioGroup = findViewById(R.id.sex_radio_group);
        calculatePromils = findViewById(R.id.calculate_promils_button);
    }

    private void setOnClickListeners(){
        calculatePromils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getUserInput();
                if (dataModel.getSexVariable() != 2 && dataModel.getWeightValue() != 0 && dataModel.getNumberOfBeers() != 0){

                    double promils = dataModel.calculatePromils(dataModel);

                    Intent intent = new Intent(MainActivity.this, FinalActivity.class);
                    intent.putExtra("PROMILS_VALUE", promils);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Uzupełnij płeć", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void getUserInput(){
        if (!weightValueEditText.getText().toString().equals("") && !numberOfBeersEditText.getText().toString().equals("")){
            dataModel.setWeightValue(Integer.parseInt(weightValueEditText.getText().toString()));
            dataModel.setNumberOfBeers(Integer.parseInt(numberOfBeersEditText.getText().toString()));
        } else {
            Toast.makeText(MainActivity.this, "Uzupełnij wagę i ilość piw.", Toast.LENGTH_SHORT).show();
        }

        int selectedSex = sexRadioGroup.getCheckedRadioButtonId();
        if (selectedSex == R.id.male_sex){
            dataModel.setSexVariable(0);
        } else if (selectedSex == R.id.female_sex){
            dataModel.setSexVariable(1);
        } else {
            dataModel.setSexVariable(2);
        }
    }
}
