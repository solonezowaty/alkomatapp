package com.solonezowaty.drinkapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class FinalActivity extends AppCompatActivity {

    private TextView numberOfPromils;
    private TextView timeToBeReadyToGo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);
        getLayoutComponents();

        double promils = getIntent().getDoubleExtra("PROMILS_VALUE", 0);

        setPromilsText(promils);
        if (promils < 0.2){
            timeToBeReadyToGo.setText(getString(R.string.driving_approval));
        } else {
            calculateTimeToBeReady(promils);
        }
    }

    private void getLayoutComponents(){
        numberOfPromils = findViewById(R.id.number_of_promils_text);
        timeToBeReadyToGo = findViewById(R.id.time_to_go_text);
    }

    private void setPromilsText(double promils){
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        String string = nf.format(promils);

        numberOfPromils.setText(string + " promila");
    }

    private void calculateTimeToBeReady(double promils){
        double numberOfMinutes = (promils/0.0016);
        long time = (long) numberOfMinutes * 60000;
        timeToBeReadyToGo.setText(getDateFromMilis(time) + " minut");
    }

    private String getDateFromMilis(long time){
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(time);
    }
}
